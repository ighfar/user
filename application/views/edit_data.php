<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title ?></title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
</head>
<body>

    <div class="container" style="margin-top: 80px">
        <div class="col-md-12">
            <?php echo form_open('user/update') ?>
  
              <div class="form-group">
                <label for="text">Nama</label>
                <input type="text" name="nama"   value="<?php echo $data_user->nama ?>" class="form-control" placeholder="Masukkan Nama">
                    <input type="hidden" name="id"   value="<?php echo $data_user->id ?>" class="form-control" >
              </div>

              <div class="form-group">
                <label for="text">Email</label>
                <input type="text" name="email"  value="<?php echo $data_user->email ?>" class="form-control" placeholder="Masukkan Email">
              </div>

              <div class="form-group">
                <label for="text">Password</label>
                <input type="text" name="password"  value="<?php echo $data_user->password ?>" class="form-control" >
              </div>
                   <div class="form-group">
                <label for="text">Tanggal Daftar</label>
                <input type="text" name="created_at"  value="<?php echo $data_user->created_at ?>" class="form-control" >
              </div>

              <div class="form-group">
                <label for="text">Role</label>
              <select name="role">
<option value="Super Admin">Super Admin</option>
<option value="Admin">Admin</option>
<option value="User">User</option>
</select>
              </div>

              <button type="submit" class="btn btn-md btn-success">Simpan</button>
              <button type="reset" class="btn btn-md btn-warning">reset</button>
            <?php echo form_close() ?>
        </div>
    </div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
</body>
</html>