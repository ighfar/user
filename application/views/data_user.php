<!DOCTYPE html>
<html>
<head>
    <title><?php echo $title ?></title>
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
</head>
<body>

    <div class="container" style="margin-top: 80px">
        <?php echo $this->session->flashdata('notif') ?>
        <a href="<?php echo base_url() ?>user/tambah/" class="btn btn-md btn-success">Tambah User</a>
        <hr>
        
    <div class="container" style="margin-top: 10px">
        <div class="col-md-12">
            <?php echo form_open('user/filter') ?>

             <div class="row">
                            <div class="col-sm-3">
                                <label class="label-filter">Tanggal Awal</label>
                            </div>
                         
                           
                              <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                       <input class="form-control datepicker" id="filter-after-date" name="from" value="<?php echo isset($_GET['from']) ? $_GET['from'] : ''?>">
            </div>
            
                            </div>
                           </div>
                <div class="col-sm-3">
                     
                                <label class="label-filter">Tanggal Akhir</label>
                            </div>
                         
                            <div class="col-sm-3">
                                <div class="form-group">
                                    <div class="form-line">
                                        <input class="form-control datepicker" id="filter-before-date" name="to" value="<?php echo isset($_GET['to']) ? $_GET['to'] : ''?>">
                                            
            </div>
            
            
           
                                </div></div>
              <div class="col-sm-3">
                    <button type="submit" class="btn btn-md btn-success">Filter</button></div>
                  
                     </div></div></div>
        
        <div class="col-md-4">
            <?php echo form_open('user/filter_role') ?>

             <div class="row">
                 <label for="text">Role</label><br></div>
      <p><input type='checkbox' name='role1' value='Super Admin' />Super Admin</p><br>
      <p><input type='checkbox' name='role2' value='Admin' />Admin</p><br>
      <p><input type='checkbox' name='role3' value='user' />User</p>
                  
              </div>
              <button type="submit" class="btn btn-md btn-success">Filter</button>
           
            <?php echo form_close() ?>
        </div>
    </div>
        
        <hr>

        <!-- table -->
        <div class="table-responsive">
            <table id="table" class="table table-striped table-bordered table-hover">
                <thead>
                  <tr>
                    <th>No.</th>
                    <th>Nama</th>
                    <th>Email</th>
                    <th>Tanggal Daftar</th>
                    <th>Role</th>
                    <th>Options</th>
                  </tr>
                </thead>
                <tbody>

                <?php
                    $no = 1; 
                    foreach($data_user as $hasil){ 
                ?>

                  <tr>
                    <td><?php echo $no++ ?></td>
                    <td><?php echo $hasil->nama ?></td>
                    <td><?php echo $hasil->email ?></td>
                    <td><?php echo $hasil->created_at ?></td>
                    <td><?php echo $hasil->role ?></td>
                    <td>
                        <a href="<?php echo base_url() ?>user/edit/<?php echo $hasil->id ?>" class="btn btn-sm btn-success">Edit</a>
                        <a href="<?php echo base_url() ?>user/hapus/<?php echo $hasil->id ?>" class="btn btn-sm btn-danger">Hapus</a>
                    </td>
                  </tr>

                <?php } ?>

                </tbody>
              </table>
        </div>

    </div>

<script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
<script>
    $('#table').DataTable( {
    autoFill: true
} );
</script>
</body>
</html>