<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Model_user extends CI_model{

    public function get_all()
    {
        $query = $this->db->select("*")
                 ->from('user')
                 ->order_by('nama', 'ASC')
                 ->get();
        return $query->result();
    }

    public function simpan($data)
    {

        $query = $this->db->insert("user", $data);

        if($query){
            return true;
        }else{
            return false;
        }

    }

    public function edit($id)
    {

        $query = $this->db->where("id", $id)
                ->get("user");

        if($query){
            return $query->row();
        }else{
            return false;
        }

    }

    public function update($data, $id)
    {

        $query = $this->db->update("user", $data, $id);

        if($query){
            return true;
        }else{
            return false;
        }

    }

    public function hapus($id)
    {

        $query = $this->db->delete("user", $id);

        if($query){
            return true;
        }else{
            return false;
        }

    }
    
      function filter($tgl1, $tgl2)
    {
    
        

        $qry = $this->db->select("*")
           ->from('user')
            ->where("created_at BETWEEN '" . $tgl1 . "' AND '" . $tgl2 . "'")
            ->group_by('id')
             ->order_by('id', 'ASC')
             ->get();

        return $qry;
    }


    public function role1()
    {
        $query = $this->db->select("*")
                 ->from('user')
                 ->where('role','Super Admin')
                 ->order_by('nama', 'ASC')
                 ->get();
        return $query->result();
    }
        public function role2()
    {
        $query = $this->db->select("*")
                 ->from('user')
                 ->where('role','Admin')
                 ->order_by('nama', 'ASC')
                 ->get();
        return $query->result();
    }
    
 public function role3()
    {
        $query = $this->db->select("*")
                 ->from('user')
                 ->where('role','User')
                 ->order_by('nama', 'ASC')
                 ->get();
        return $query->result();
    }



}

