<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    public function __construct(){

        parent ::__construct();

        //load model
        $this->load->model('model_user'); 

    }

    public function index()
    {
        $data = array(

            'title'     => 'Data User',
            'data_user' => $this->model_user->get_all(),

        );

        $this->load->view('data_user', $data);
    }

    public function tambah()
    {
        $data = array(

            'title'     => 'Tambah Data User'

        );

        $this->load->view('tambah_data', $data);
    }

    public function simpan()
    {
        $data = array(

            'nama'       => $this->input->post("nama"),
            'email'         => $this->input->post("email"),
            'password'    => $this->input->post("password"),
            'role'         => $this->input->post("role"),

        );

        $this->model_user->simpan($data);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil disimpan didatabase.
                                                </div>');

        //redirect
        redirect('user/');

    }

    public function edit($id)
    {
        $id_buku = $this->uri->segment(3);

        $data = array(

            'title'     => 'Edit Data User',
            'data_user' => $this->model_user->edit($id)

        );

        $this->load->view('edit_data', $data);
    }

    public function update()
    {
        $id['id'] = $this->input->post("id");
        $data = array(

            'nama'       => $this->input->post("nama"),
            'email'         => $this->input->post("email"),
            'password'    => $this->input->post("password"),
            'role'         => $this->input->post("role"),

        );

        $this->model_user->update($data, $id);

        $this->session->set_flashdata('notif', '<div class="alert alert-success alert-dismissible"> Success! data berhasil diupdate didatabase.
                                                </div>');

        //redirect
        redirect('user/');

    }

    public function hapus($id)
    {
        $id['id'] = $this->uri->segment(3);

        $this->model_user->hapus($id);

        //redirect
        redirect('user/');

    }
    
      public function filter()
    {
          
     if(isset($_GET['from']) && isset($_GET['to']) ) {
        $tgl1 = date('Y-m-d', strtotime($_GET['from'])); 
        $tgl2 = date('Y-m-d', strtotime($_GET['to'])); 

       
             $data = array(

            'title'     => 'Filter User',
            'data_user' => $this->model_user->filter($tgl1,$tgl2),

        );

        $this->load->view('data_user', $data);
    }
      }
    public function filter_role()
    {
        $check1 = $this->input->post('role1');
        $check2 = $this->input->post('role2');
        $check3 = $this->input->post('role3');
        
        if ($check1.checked == "true"){
            
             $data = array(

            'title'     => 'Data User',
            'data_user' => $this->model_user->role1(),

        );
             $this->load->view('data_user', $data);

        }else  if ($check2.checked == "true"){
            
             $data = array(

            'title'     => 'Data User',
            'data_user' => $this->model_user->role2(),

        );
             $this->load->view('data_user', $data);

        }else   if ($check3.checked == "true"){
            
             $data = array(

            'title'     => 'Data User',
            'data_user' => $this->model_user->role3(),

        );
             $this->load->view('data_user', $data);

        }
            

    }
}
